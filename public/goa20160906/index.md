% Development using goa and golang on Pacificporter inc.
% HARUYAMA Seigo(@haruyama)

# API library before using goa

We adopted and use [ant0ine/go-json-rest](https://github.com/ant0ine/go-json-rest)

We have some problems

* Routing 
* Validation 
* ...

# goa testing

We do not use "github.com/goadesign/goa/goatest".

We use "github.com/ivpusic/httpcheck".

# goa + swagger-ui

[swagger-api/swagger-ui](https://github.com/swagger-api/swagger-ui)

```go
func MountController(service *goa.Service) error {
	uiPath := filepath.Join(system.RelaxBasePath, "apiserver/swaggerui/dist")
	uiIndex := filepath.Join(uiPath, "index.html")

	if err := service.ServeFiles("/swagger-ui/", uiIndex); err != nil {
		return err
	}
	return service.ServeFiles("/swagger-ui/*filepath", uiPath)
}
```

# goa + HTTP Security Headers

```go
var xFrameOptions = http.CanonicalHeaderKey("X-Frame-Options")
var xXSSProtection = http.CanonicalHeaderKey("X-XSS-Protection")
var xContentTypeOptions = http.CanonicalHeaderKey("X-Content-Type-Options")
var contentSecurityPolicy = http.CanonicalHeaderKey("Content-Security-Policy")

func SecurityHeaders() goa.Middleware {
	return func(h goa.Handler) goa.Handler {
		return func(ctx context.Context, rw http.ResponseWriter, r *http.Request) error {
			rw.Header().Set(xFrameOptions, "DENY")
			rw.Header().Set(xXSSProtection, "1; mode=block")
			rw.Header().Set(xContentTypeOptions, "nosniff")
			rw.Header().Set(contentSecurityPolicy, "default-src 'self'; script-src 'self' 'unsafe-eval' 
				'unsafe-inline'; style-src 'self' 'unsafe-inline'; font-src 'self' data:; img-src 'self' data:;")
			return h(ctx, rw, r)
		}
	}
}
```

# goa + AWS ELB

based on [zenzen/goji:/web/middleware/realip.go](https://github.com/zenazn/goji/blob/845982030542a0fd9c8e8cbf76a84c7482cb3755/web/middleware/realip.go)

```go
func RealIP() goa.Middleware {
	return func(h goa.Handler) goa.Handler {
		return func(ctx context.Context, rw http.ResponseWriter, r *http.Request) error {
			if rip := realIP(r); rip != "" {
				r.RemoteAddr = rip
			}
			return h(ctx, rw, r)
		}
	}
}
```

# CI

We use [Wercker](http://wercker.com/).

* [pacificporter/box-golang-docker:Dockerfile](https://github.com/pacificporter/box-golang-docker/blob/master/Dockerfile)

# Package management

We use [glide](https://github.com/Masterminds/glide).

We used [tools/godep: dependency tool for go](https://github.com/tools/godep/) before using vendoring.


# Static code analysis tools

We use following tools:

* go vet
* [errcheck](https://github.com/kisielk/errcheck)
* [golint](https://github.com/golang/lint) / golintx
* [sigma/gocyclo](github.com/sigma/gocyclo)
* [unconvert](https://github.com/mdempsky/unconvert)
* [gosimple](https://github.com/dominikh/go-simple)

# golintx

* [haruyama/golintx](https://github.com/haruyama/golintx)

# Differences from original golint

* Support per-directory config files
* exit(1) if any problem exists
* Support multi directories on command line
    * ex: golintx $(glide novendor)

# DB libraries

* [jinzhu/gorm](https://github.com/jinzhu/gorm)
* [go-pg/pg at v3](https://github.com/go-pg/pg/tree/v3)

# DB migration tools

* [liamstask / goose](https://bitbucket.org/liamstask/goose/)
* [rubenv/sql-migrate](https://github.com/rubenv/sql-migrate)

# Other libraries

* [zenazn/goji](https://github.com/zenazn/goji/)
    * New project: [goji/goji](https://github.com/goji/goji/)
* [jmcvetta/napping: Golang HTTP client library](https://github.com/jmcvetta/napping/)
	* "Package napping is a Go client library for interacting with RESTful APIs"
* [pacificporter/surf](https://github.com/pacificporter/surf/)
    * Original: [headzoo/surf](https://github.com/headzoo/surf)

# Other tools

* [pacificporter/goshim](https://github.com/pacificporter/goshim)
	* Original: [Songmu/goshim](https://github.com/Songmu/goshim/)
    * Add -v, -f flags
